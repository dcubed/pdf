package object

import (
	"strconv"
)

// Float contains a floating point number.
type Float struct {
	value float64
}

// NewFloat creates a new float object.
func NewFloat(value float64) *Float {
	return &Float{value: value}
}

// Value returns the float value.
func (f *Float) Value() interface{} {
	return f.value
}

// Serialize encodes the float.
func (f *Float) Serialize() ([]byte, error) {
	return []byte(strconv.FormatFloat(f.value, 'f', -1, 64)), nil
}

// Parse reads the integer value from the given bytes.
func (f *Float) Parse(data []byte) error {
	parsedFloat, err := strconv.ParseFloat(string(data), 64)
	if err != nil {
		f.value = 0
		return err
	}

	f.value = parsedFloat
	return nil
}
