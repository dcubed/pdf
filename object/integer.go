package object

import (
	"strconv"
)

// Integer is an object containing a single signed integer.
type Integer struct {
	value int64
}

// NewInteger creates a new Integer object with the given value.
func NewInteger(value int64) *Integer {
	return &Integer{value: value}
}

// Value returns the integer value.
func (i *Integer) Value() interface{} {
	return i.value
}

// Serialize encodes the integer.
func (i *Integer) Serialize() ([]byte, error) {
	return []byte(strconv.FormatInt(i.value, 10)), nil
}

// Parse reads the integer value from the given bytes.
func (i *Integer) Parse(data []byte) error {
	parsedInt, err := strconv.ParseInt(string(data), 10, 64)
	if err != nil {
		i.value = 0
		return err
	}

	i.value = parsedInt
	return nil
}
