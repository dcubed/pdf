// Package object implements the fundamental PDF object types
package object

// Generic is the interface that all PDF objects implement.
type Generic interface {

	// Value returns the underlying value of this object, which depends on the object's type.
	Value() interface{}

	// Serialize returns the object contents for storage in a PDF file.
	Serialize() ([]byte, error)

	// Parse extracts the value and stores it in this object from the PDF contents.
	Parse(data []byte) error
}
