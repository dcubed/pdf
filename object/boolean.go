package object

import "errors"

// True & False Keywords
const trueKeyword = "true"
const falseKeyword = "false"

// Boolean objects contain either the value of true or false.
type Boolean struct {
	value bool
}

// NewBoolean creates a boolean object with the given value.
func NewBoolean(value bool) *Boolean {
	return &Boolean{value: value}
}

// Value returns the boolean's value.
func (b *Boolean) Value() interface{} {
	return b.value
}

// Serialize returns the ASCII sequence for "true" or "false".
func (b *Boolean) Serialize() ([]byte, error) {
	if b.value {
		return []byte(trueKeyword), nil
	}
	return []byte(falseKeyword), nil
}

// Parse extracts the true or false value from the bytes.
func (b *Boolean) Parse(data []byte) error {
	stringData := string(data)

	if stringData == trueKeyword {
		b.value = true
	} else if stringData == falseKeyword {
		b.value = false
	} else {
		b.value = false
		return errors.New("Invalid boolean object data")
	}

	return nil
}
