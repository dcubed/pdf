package test

import (
	"testing"

	"gitlab.com/dcubed/pdf/object"
)

func TestLiteralString_Parse_no_escape(t *testing.T) {
	str := object.NewLiteralString(make([]byte, 0, 10))
	var err error

	// no-escape, no-newline
	err = str.Parse([]byte("(test)"))
	if err != nil {
		t.Fatalf("Unable to parse no-escape no-newline string: %v", err)
	}
	if string(str.Value().([]byte)) != "test" {
		t.Fatalf(`Expected "%s" but got "%s" after parse`, "test", string(str.Value().([]byte)))
	}

	// no-escape, \r
	err = str.Parse([]byte("(one\x0Dtwo)"))
	if err != nil {
		t.Fatalf("Unable to parse no-escape newline string: %v", err)
	}
	if string(str.Value().([]byte)) != "one\x0Atwo" {
		t.Fatalf(`Expected "%s" but got "%s" after parse`, "one\x0Atwo", string(str.Value().([]byte)))
	}

	// no-escape, \n
	err = str.Parse([]byte("(one\x0Atwo)"))
	if err != nil {
		t.Fatalf("Unable to parse no-escape newline string: %v", err)
	}
	if string(str.Value().([]byte)) != "one\x0Atwo" {
		t.Fatalf(`Expected "%s" but got "%s" after parse`, "one\x0Atwo", string(str.Value().([]byte)))
	}

	// no-escape, \r\n
	err = str.Parse([]byte("(one\x0D\x0Atwo)"))
	if err != nil {
		t.Fatalf("Unable to parse no-escape newline string: %v", err)
	}
	if string(str.Value().([]byte)) != "one\x0Atwo" {
		t.Fatalf(`Expected "%s" but got "%s" after parse`, "one\x0Atwo", string(str.Value().([]byte)))
	}

	// no-escape, empty
	err = str.Parse([]byte("()"))
	if err != nil {
		t.Fatalf("Unable to parse empty string: %v", err)
	}
	if string(str.Value().([]byte)) != "" {
		t.Fatalf(`Expected "%s" but got "%s" after parse`, "", string(str.Value().([]byte)))
	}

	// balanced parenthesis
	err = str.Parse([]byte("(one(two(two)one)...)"))
	if err != nil {
		t.Fatalf("Unable to parse string: %v", err)
	}
	if string(str.Value().([]byte)) != "one(two(two)one)..." {
		t.Fatalf(`Expected "%s" but got "%s" after parse`, "one(two(two)one)...", string(str.Value().([]byte)))
	}

}

func TestLiteralString_Parse_invalid(t *testing.T) {
	str := object.NewLiteralString(make([]byte, 0, 10))

	// missing parenthesis
	err := str.Parse([]byte("(hi there, ..."))
	if err == nil {
		t.Fatal("Expected error during parse but got nil")
	}

	// un-balanced parenthesis
	err = str.Parse([]byte("(one(two(...)"))
	if err == nil {
		t.Fatal("Expected error while parsing un-balanced parenthesis but got nil")
	}

	// empty
	err = str.Parse([]byte(""))
	if err == nil {
		t.Fatal(`Expected error while parsing "" but got nil`)
	}

	// escaped end
	err = str.Parse([]byte("(\\)"))
	if err == nil {
		t.Fatal(`Expected error when the closing parenthesis is escaped but go nil`)
	}
}

func TestLiteralString_Parse_standard_escapes(t *testing.T) {
	str := object.NewLiteralString(make([]byte, 0, 10))
	var err error

	// \n
	err = str.Parse([]byte("(\\n)"))
	if err != nil {
		t.Fatalf("Unable to parse string: %v", err)
	}
	if string(str.Value().([]byte)) != "\n" {
		t.Fatalf(`Expected %q but got %q after parse`, "\n", string(str.Value().([]byte)))
	}

	// \r
	err = str.Parse([]byte("(\\r)"))
	if err != nil {
		t.Fatalf("Unable to parse string: %v", err)
	}
	if string(str.Value().([]byte)) != "\r" {
		t.Fatalf(`Expected %q but got %q after parse`, "\r", string(str.Value().([]byte)))
	}

	// \t
	err = str.Parse([]byte("(\\t)"))
	if err != nil {
		t.Fatalf("Unable to parse string: %v", err)
	}
	if string(str.Value().([]byte)) != "\t" {
		t.Fatalf(`Expected %q but got %q after parse`, "\r", string(str.Value().([]byte)))
	}

	// \b
	err = str.Parse([]byte("(\\b)"))
	if err != nil {
		t.Fatalf("Unable to parse string: %v", err)
	}
	if string(str.Value().([]byte)) != "\x08" {
		t.Fatalf(`Expected %q but got %q after parse`, "\x08", string(str.Value().([]byte)))
	}

	// \f
	err = str.Parse([]byte("(\\f)"))
	if err != nil {
		t.Fatalf("Unable to parse string: %v", err)
	}
	if string(str.Value().([]byte)) != "\x0C" {
		t.Fatalf(`Expected %q but got %q after parse`, "\x0C", string(str.Value().([]byte)))
	}

	// \(
	err = str.Parse([]byte("(\\()"))
	if err != nil {
		t.Fatalf("Unable to parse string: %v", err)
	}
	if string(str.Value().([]byte)) != "(" {
		t.Fatalf(`Expected %q but got %q after parse`, "(", string(str.Value().([]byte)))
	}

	// \)
	err = str.Parse([]byte("(\\))"))
	if err != nil {
		t.Fatalf("Unable to parse string: %v", err)
	}
	if string(str.Value().([]byte)) != ")" {
		t.Fatalf(`Expected %q but got %q after parse`, ")", string(str.Value().([]byte)))
	}

	// \\
	err = str.Parse([]byte("(\\\\)"))
	if err != nil {
		t.Fatalf("Unable to parse string: %v", err)
	}
	if string(str.Value().([]byte)) != "\\" {
		t.Fatalf(`Expected %q but got %q after parse`, "\\", string(str.Value().([]byte)))
	}

}

func TestLiteralString_Parse_newline_escapes(t *testing.T) {
	str := object.NewLiteralString(make([]byte, 0, 10))
	var err error

	// \r
	err = str.Parse([]byte("(a\\\rb)"))
	if err != nil {
		t.Fatalf("Unable to parse string: %v", err)
	}
	if string(str.Value().([]byte)) != "ab" {
		t.Fatalf(`Expected %q but got %q after parse`, "ab", string(str.Value().([]byte)))
	}

	// \n
	err = str.Parse([]byte("(a\\\nb)"))
	if err != nil {
		t.Fatalf("Unable to parse string: %v", err)
	}
	if string(str.Value().([]byte)) != "ab" {
		t.Fatalf(`Expected %q but got %q after parse`, "ab", string(str.Value().([]byte)))
	}

	// \r\n
	err = str.Parse([]byte("(a\\\r\nb)"))
	if err != nil {
		t.Fatalf("Unable to parse string: %v", err)
	}
	if string(str.Value().([]byte)) != "ab" {
		t.Fatalf(`Expected %q but got %q after parse`, "ab", string(str.Value().([]byte)))
	}

}

func TestLiteralString_Parse_codepoint_escapes(t *testing.T) {
	str := object.NewLiteralString(make([]byte, 0, 10))
	var err error

	// 3 digit
	err = str.Parse([]byte("(\\200a)"))
	if err != nil {
		t.Fatalf("Unable to parse string: %v", err)
	}
	if string(str.Value().([]byte)) != "\x80a" {
		t.Fatalf(`Expected %q but got %q after parse`, "\x80a", string(str.Value().([]byte)))
	}

	// 2 digit
	err = str.Parse([]byte("(\\20a)"))
	if err != nil {
		t.Fatalf("Unable to parse string: %v", err)
	}
	if string(str.Value().([]byte)) != "\x10a" {
		t.Fatalf(`Expected %q but got %q after parse`, "\x10a", string(str.Value().([]byte)))
	}

	// 1 digit
	err = str.Parse([]byte("(\\2a)"))
	if err != nil {
		t.Fatalf("Unable to parse string: %v", err)
	}
	if string(str.Value().([]byte)) != "\x02a" {
		t.Fatalf(`Expected %q but got %q after parse`, "\x02a", string(str.Value().([]byte)))
	}

	// 4 digit
	err = str.Parse([]byte("(\\2009)"))
	if err != nil {
		t.Fatalf("Unable to parse string: %v", err)
	}
	if string(str.Value().([]byte)) != "\x809" {
		t.Fatalf(`Expected %q but got %q after parse`, "\x809", string(str.Value().([]byte)))
	}

	// end of string
	err = str.Parse([]byte("(\\2)"))
	if err != nil {
		t.Fatalf("Unable to parse string: %v", err)
	}
	if string(str.Value().([]byte)) != "\x02" {
		t.Fatalf(`Expected %q but got %q after parse`, "\x02", string(str.Value().([]byte)))
	}
	err = str.Parse([]byte("(\\02)"))
	if err != nil {
		t.Fatalf("Unable to parse string: %v", err)
	}
	if string(str.Value().([]byte)) != "\x02" {
		t.Fatalf(`Expected %q but got %q after parse`, "\x02", string(str.Value().([]byte)))
	}
	err = str.Parse([]byte("(\\002)"))
	if err != nil {
		t.Fatalf("Unable to parse string: %v", err)
	}
	if string(str.Value().([]byte)) != "\x02" {
		t.Fatalf(`Expected %q but got %q after parse`, "\x02", string(str.Value().([]byte)))
	}

}

func TestLiteralString_Serialize(t *testing.T) {
	var str *object.LiteralString
	var err error
	var out []byte

	// empty
	str = object.NewLiteralString([]byte(""))
	out, err = str.Serialize()
	if err != nil {
		t.Fatalf("Unable to serialize string: %v", err)
	}
	if string(out) != "()" {
		t.Fatalf("Expected %q but got %q after serialize", "()", string(out))
	}

	// normal
	str = object.NewLiteralString([]byte("hello\nthere"))
	out, err = str.Serialize()
	if err != nil {
		t.Fatalf("Unable to serialize string: %v", err)
	}
	if string(out) != "(hello\nthere)" {
		t.Fatalf("Expected %q but got %q after serialize", "(hello\nthere)", string(out))
	}

	// esaping
	str = object.NewLiteralString([]byte(")\\("))
	out, err = str.Serialize()
	if err != nil {
		t.Fatalf("Unable to serialize string: %v", err)
	}
	if string(out) != "(\\)\\\\\\()" {
		t.Fatalf("Expected %q but got %q after serialize", "(\\)\\\\\\()", string(out))
	}

	// UTF-8
	str = object.NewLiteralString([]byte("\u2318"))
	out, err = str.Serialize()
	if err != nil {
		t.Fatalf("Unable to serialize string: %v", err)
	}
	if string(out) != "(\u2318)" {
		t.Fatalf("Expected %q but got %q after serialize", "(\u2318)", string(out))
	}

}
