package test

import (
	"testing"

	"gitlab.com/dcubed/pdf/object"
)

func TestFloat_Serialize_positive(t *testing.T) {
	data, err := object.NewFloat(1.23).Serialize()
	if err != nil {
		t.Fatal(err)
	}

	if string(data) != "1.23" {
		t.Fatalf(`Expected "1.23" got "%v"`, string(data))
	}
}

func TestFloat_Serialize_negative(t *testing.T) {
	data, err := object.NewFloat(-3.21).Serialize()
	if err != nil {
		t.Fatal(err)
	}

	if string(data) != "-3.21" {
		t.Fatalf(`Expected "-3.21" got "%v"`, string(data))
	}
}

func TestFloat_Parse_positive(t *testing.T) {
	obj := object.NewFloat(0)

	err := obj.Parse([]byte("+1"))
	if err != nil {
		t.Fatal(err)
	}
	if obj.Value().(float64) != +1.0 {
		t.Fatalf(`Expected 1 got %f`, obj.Value())
	}

	err = obj.Parse([]byte("+0.1"))
	if err != nil {
		t.Fatal(err)
	}
	if obj.Value().(float64) != +0.1 {
		t.Fatalf(`Expected 0.1 got %f`, obj.Value())
	}

	err = obj.Parse([]byte("+.1"))
	if err != nil {
		t.Fatal(err)
	}
	if obj.Value().(float64) != +0.1 {
		t.Fatalf(`Expected 0.1 got %f`, obj.Value())
	}

	err = obj.Parse([]byte("+1."))
	if err != nil {
		t.Fatal(err)
	}
	if obj.Value().(float64) != +1.0 {
		t.Fatalf(`Expected 1 got %f`, obj.Value())
	}
}

func TestFloat_Parse_negative(t *testing.T) {
	obj := object.NewFloat(0)

	err := obj.Parse([]byte("-1"))
	if err != nil {
		t.Fatal(err)
	}
	if obj.Value().(float64) != -1.0 {
		t.Fatalf(`Expected -1 got %f`, obj.Value())
	}

	err = obj.Parse([]byte("-0.1"))
	if err != nil {
		t.Fatal(err)
	}
	if obj.Value().(float64) != -0.1 {
		t.Fatalf(`Expected -0.1 got %f`, obj.Value())
	}

	err = obj.Parse([]byte("-.1"))
	if err != nil {
		t.Fatal(err)
	}
	if obj.Value().(float64) != -0.1 {
		t.Fatalf(`Expected -0.1 got %f`, obj.Value())
	}

	err = obj.Parse([]byte("-1."))
	if err != nil {
		t.Fatal(err)
	}
	if obj.Value().(float64) != -1.0 {
		t.Fatalf(`Expected -1 got %f`, obj.Value())
	}
}
