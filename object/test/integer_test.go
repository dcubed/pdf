package test

import (
	"testing"

	"gitlab.com/dcubed/pdf/object"
)

func TestInteger_Serialize_positive(t *testing.T) {
	obj := object.NewInteger(43445)

	val, err := obj.Serialize()
	if err != nil {
		t.Fatalf("Unable to serialize integer object: %v", err)
	}

	if string(val) != "\x34\x33\x34\x34\x35" {
		t.Fatalf(`Expected "43445" got "%s"`, val)
	}
}

func TestInteger_Serialize_negative(t *testing.T) {
	obj := object.NewInteger(-98)

	val, err := obj.Serialize()
	if err != nil {
		t.Fatalf("Unable to serialize integer object: %v", err)
	}

	if string(val) != "\x2D\x39\x38" {
		t.Fatalf(`Expected "-98" got "%s"`, val)
	}
}

func TestInteger_Serialize_zero(t *testing.T) {
	obj := object.NewInteger(0)

	val, err := obj.Serialize()
	if err != nil {
		t.Fatalf("Unable to serialize integer object: %v", err)
	}

	if string(val) != "\x30" {
		t.Fatalf(`Expected "0" got "%s"`, val)
	}
}

func TestInteger_Parse_valid(t *testing.T) {
	obj := object.NewInteger(0)

	err := obj.Parse([]byte("\x2D\x31\x32\x33"))
	if err != nil {
		t.Fatalf("Unable to parse integer: %v", err)

	}

	if obj.Value().(int64) != -123 {
		t.Fatalf("Expected parsed value of -123 got %v", obj.Value())
	}
}

func TestInteger_Parse_invalid(t *testing.T) {
	obj := object.NewInteger(123)

	err := obj.Parse([]byte("\x42\x43\x44"))
	if err == nil {
		t.Fatal("Expected error during parse but error was nil")

	}

	if obj.Value().(int64) != 0 {
		t.Fatalf("Expected parsed value set to 0 on error but got %v", obj.Value())
	}
}
