package test

import (
	"testing"

	"gitlab.com/dcubed/pdf/object"
)

func TestBoolean_Serialize_true(t *testing.T) {
	obj := object.NewBoolean(true)
	boolData, err := obj.Serialize()

	if err != nil {
		t.Fatalf(`Unable to serialize boolean: %v`, err)
	}
	if string(boolData) != "\x74\x72\x75\x65" {
		t.Fatalf(`Expected "true" got "%s"`, string(boolData))
	}
}

func TestBoolean_Serialize_false(t *testing.T) {
	obj := object.NewBoolean(false)
	boolData, err := obj.Serialize()

	if err != nil {
		t.Fatalf(`Unable to serialize boolean: %v`, err)
	}
	if string(boolData) != "\x66\x61\x6C\x73\x65" {
		t.Fatalf(`Expected "false" got "%s"`, string(boolData))
	}
}

func TestBoolean_Parse_true(t *testing.T) {
	obj := object.NewBoolean(false)

	err := obj.Parse([]byte("\x74\x72\x75\x65"))
	if err != nil {
		t.Fatalf("Unable to parse boolean: %v", err)
	}

	if obj.Value().(bool) != true {
		t.Fatal("Value was not true")
	}
}

func TestBoolean_Parse_false(t *testing.T) {
	obj := object.NewBoolean(true)

	err := obj.Parse([]byte("\x66\x61\x6C\x73\x65"))
	if err != nil {
		t.Fatalf("Unable to parse boolean: %v", err)
	}

	if obj.Value().(bool) != false {
		t.Fatal("Value was not false")
	}
}

func TestBoolean_Parse_invalid(t *testing.T) {
	obj := object.NewBoolean(true)

	err := obj.Parse([]byte("\x42\x43\x44"))
	if err == nil {
		t.Fatal("Expected error on invalid parse but error was nil")
	}

	if obj.Value().(bool) != false {
		t.Fatal("Value should be set to false on error but was true")
	}
}
