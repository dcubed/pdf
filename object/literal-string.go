package object

import (
	"errors"
)

// LiteralString is a string that is not hex encoded.
type LiteralString struct {
	value []byte
}

// NewLiteralString creates a new LiteralString object.
func NewLiteralString(value []byte) *LiteralString {
	return &LiteralString{value: value}
}

// Value returns the string.
func (s *LiteralString) Value() interface{} {
	return s.value
}

// Serialize returns the literal string encoded for use in a file.
func (s *LiteralString) Serialize() ([]byte, error) {
	encoded := make([]byte, 0, len(s.value)+2)
	encoded = append(encoded, '(')

	for _, val := range s.value {
		if val == '\\' {
			encoded = append(encoded, '\\', '\\')
		} else if val == '(' {
			encoded = append(encoded, '\\', '(')
		} else if val == ')' {
			encoded = append(encoded, '\\', ')')
		} else {
			encoded = append(encoded, val)
		}
	}

	encoded = append(encoded, ')')
	return encoded, nil
}

// Parse extracts the literal string from the slice of data.
func (s *LiteralString) Parse(data []byte) error {
	var err error

	if len(data) < 2 {
		return errors.New("Literal string must be at least 2 bytes long")
	}
	if data[0] != '\x28' || data[len(data)-1] != '\x29' {
		return errors.New("Literal string must start with ( and end with )")
	}

	s.value = make([]byte, 0, len(data)-2)
	inEscape := false
	currentCode := make([]byte, 0, 3)
	parenthesisLevel := 0
	for i := 1; i < len(data)-1; i++ {
		if inEscape {

			// escaped codepoint
			if data[i] >= '\x30' && data[i] <= '\x39' {
				currentCode = append(currentCode, data[i])
				if len(currentCode) > 2 { // 3 digit number
					s.value, err = appendOctalValue(string(currentCode), s.value)
					if err != nil {
						return err
					}
					currentCode = currentCode[:0]
					inEscape = false
				}
				continue

			}

			// less than 3 digit codepoint
			if len(currentCode) > 0 {
				s.value, err = appendOctalValue(string(currentCode), s.value)
				if err != nil {
					return err
				}
				currentCode = currentCode[:0]
				inEscape = false

				// repeat this byte, it is not part of the escaped value
				i--
				continue

			}

			// escaped newline
			if newlineSize := isLineEnd(i, data); newlineSize > 0 {
				i += newlineSize - 1
				inEscape = false
				continue
			}

			// standard escape
			switch data[i] {
			case '\x6E': // \n
				s.value = append(s.value, '\x0A')
				break
			case '\x72': // \r
				s.value = append(s.value, '\x0D')
				break
			case '\x74': // \t
				s.value = append(s.value, '\x09')
				break
			case '\x62': // \b
				s.value = append(s.value, '\x08')
				break
			case '\x66': // \f
				s.value = append(s.value, '\x0C')
				break
			case '\x28': // \(
				s.value = append(s.value, '(')
				break
			case '\x29': // \)
				s.value = append(s.value, ')')
				break
			case '\x5C': // \\
				s.value = append(s.value, '\\')
				break
			default:
				return errors.New("Invalid escaped byte in literal string")
			}
			inEscape = false
			continue

		}

		// reverse solidus
		if data[i] == '\x5C' {
			inEscape = true
			continue
		}

		// newline
		if newlineSize := isLineEnd(i, data); newlineSize > 0 {
			i += newlineSize - 1
			s.value = append(s.value, '\x0A')
			continue
		}

		// parenthesis
		if data[i] == '\x28' {
			parenthesisLevel++
		} else if data[i] == '\x29' {
			parenthesisLevel--
		}

		s.value = append(s.value, data[i])
	}

	if inEscape {
		if len(currentCode) > 0 {
			s.value, err = appendOctalValue(string(currentCode), s.value)
			if err != nil {
				return err
			}

		} else {
			return errors.New("The closing parenthesis of this literal string is escaped")
		}
	} else if parenthesisLevel != 0 {
		return errors.New("This literal string contains unbalanced un-escaped parenthesis")
	}

	return nil
}
