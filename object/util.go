package object

import (
	"errors"
	"fmt"
	"strconv"
	"unicode/utf8"

	"gitlab.com/dcubed/pdf/charset"
)

// isLineEnd returns the number of bytes to move forward to get past the line or 0 if there is not an end of line.
func isLineEnd(index int, data []byte) int {
	if index < 0 || index >= len(data) {
		return 0
	}

	// carriage return
	if data[index] == '\x0D' {
		if index+1 < len(data) && data[index+1] == '\x0A' {
			return 2
		}
		return 1
	}

	// line feed
	if data[index] == '\x0A' {
		return 1
	}

	return 0
}

// appendOctalValue appends an octal value in the string to the byte array.
func appendOctalValue(str string, buffer []byte) ([]byte, error) {
	number, err := strconv.ParseInt(str, 8, 64)
	if err != nil {
		return buffer, err
	}
	if number > 255 {
		return buffer, fmt.Errorf(`parsing %q as byte is out of range`, str)
	}
	return append(buffer, byte(number)), nil
}

// appendOctalRune appends a PDF Doc rune at a given octal code point.
func appendOctalRune(codepoint string, buffer []byte) ([]byte, error) {
	runeNum, err := strconv.ParseInt(codepoint, 8, 32)
	if err != nil {
		return buffer, err
	}

	return appendRune(int32(runeNum), buffer)
}

// appendRune appends a PDF Doc rune to the given byte slice.
func appendRune(codepoint int32, buffer []byte) ([]byte, error) {
	pdfRune, defined := charset.PDFDoc[codepoint]
	if !defined {
		return append(buffer, '?'), nil
	}

	encodeLength := utf8.RuneLen(pdfRune)
	if encodeLength < 0 {
		return buffer, errors.New("Invalid UTF-8 rune")
	}

	buf := make([]byte, encodeLength)
	bufLength := utf8.EncodeRune(buf, pdfRune)
	if bufLength != encodeLength {
		return buffer, errors.New("UTF-8 buffer is not long enough")
	}

	return append(buffer, buf...), nil
}
