[![pipeline status](https://gitlab.com/dcubed/pdf/badges/master/pipeline.svg)](https://gitlab.com/dcubed/pdf/commits/master)
[![coverage report](https://gitlab.com/dcubed/pdf/badges/master/coverage.svg)](https://gitlab.com/dcubed/pdf/commits/master)
[![GoDoc](https://godoc.org/gitlab.com/dcubed/pdf?status.svg)](https://godoc.org/gitlab.com/dcubed/pdf)

# D Cubed PDF Library
As the library we begin to create, the documentation is an empty slate...

If API reference is what you seek, then visit [GoDoc](https://godoc.org/gitlab.com/dcubed/pdf) and take a peek!